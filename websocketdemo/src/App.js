import React from "react";
import logo from "./logo.svg";
import "./App.css";
import WebSocketComponent from "./components/WebSocketComponent";

function App() {
  return (
    <div className="App">
      <img src={logo} alt="React Websocket" width={100} />
      <WebSocketComponent />
    </div>
  );
}

export default App;
